package com.tds.finartz.browser;

import org.springframework.http.ResponseEntity;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

interface BrowserClientService {
    @POST
    Call<String> callACS(@Url String methodUrl, @Body CReq rReq);
}
