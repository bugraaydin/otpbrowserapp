package com.tds.finartz.browser;

public final class CReq {

    private String threeDSServerTransID;

    private String acsTransID;

    private String challengeCancel;

    private String challengeDataEntry;

    private String challengeHTMLDataEntry;

    private String challengeWindowSize;

    private MessageExtension [] messageExtension;

    private String messageType;

    private String messageVersion;

    private String resendChallenge;

    private String sdkTransID;

    private String sdkCounterStoA;

    private String deviceChannel;


    public String getThreeDSServerTransID() {
        return threeDSServerTransID;
    }

    public void setThreeDSServerTransID(String threeDSServerTransID) {
        this.threeDSServerTransID = threeDSServerTransID;
    }

    public String getAcsTransID() {
        return acsTransID;
    }

    public void setAcsTransID(String acsTransID) {
        this.acsTransID = acsTransID;
    }

    public String getChallengeCancel() {
        return challengeCancel;
    }

    public void setChallengeCancel(String challengeCancel) {
        this.challengeCancel = challengeCancel;
    }

    public String getChallengeDataEntry() {
        return challengeDataEntry;
    }

    public void setChallengeDataEntry(String challengeDataEntry) {
        this.challengeDataEntry = challengeDataEntry;
    }

    public String getChallengeHTMLDataEntry() {
        return challengeHTMLDataEntry;
    }

    public void setChallengeHTMLDataEntry(String challengeHTMLDataEntry) {
        this.challengeHTMLDataEntry = challengeHTMLDataEntry;
    }

    public String getChallengeWindowSize() {
        return challengeWindowSize;
    }

    public void setChallengeWindowSize(String challengeWindowSize) {
        this.challengeWindowSize = challengeWindowSize;
    }

    public MessageExtension[] getMessageExtension() {
        return messageExtension;
    }

    public void setMessageExtension(MessageExtension[] messageExtension) {
        this.messageExtension = messageExtension;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageVersion() {
        return messageVersion;
    }

    public void setMessageVersion(String messageVersion) {
        this.messageVersion = messageVersion;
    }

    public String getResendChallenge() {
        return resendChallenge;
    }

    public void setResendChallenge(String resendChallenge) {
        this.resendChallenge = resendChallenge;
    }

    public String getSdkTransID() {
        return sdkTransID;
    }

    public void setSdkTransID(String sdkTransID) {
        this.sdkTransID = sdkTransID;
    }

    public String getSdkCounterStoA() {
        return sdkCounterStoA;
    }

    public void setSdkCounterStoA(String sdkCounterStoA) {
        this.sdkCounterStoA = sdkCounterStoA;
    }

    public String getDeviceChannel() {
        return deviceChannel;
    }

    public void setDeviceChannel(String deviceChannel) {
        this.deviceChannel = deviceChannel;
    }

    @Override
    public String toString(){
        String jsonString = "{" +
                "\"threeDSServerTransID\": \"%s\"," +
                "\"acsTransID\": \"%s\"," +
                "\"messageType\": \"%s\"," +
                "\"messageVersion\": \"%s\"," +
                "\"sdkTransID\": \"%s\"," +
                "\"sdkCounterStoA\": \"%s\"," +
                "\"deviceChannel\": \"%s\"" +
                "}";
        return String.format(jsonString, threeDSServerTransID, acsTransID, messageType, messageVersion,sdkTransID,sdkCounterStoA,deviceChannel);
    }
}
