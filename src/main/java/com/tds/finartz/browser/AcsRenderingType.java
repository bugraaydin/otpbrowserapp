package com.tds.finartz.browser;

public class AcsRenderingType {
    private String acsInterface;
    private String acsUiTemplate;

    public AcsRenderingType() {
    }

    public String getAcsInterface() {
        return acsInterface;
    }

    public String getAcsUiTemplate() {
        return acsUiTemplate;
    }

    public void setAcsInterface(String acsInterface) {
        this.acsInterface = acsInterface;
    }

    public void setAcsUiTemplate(String acsUiTemplate) {
        this.acsUiTemplate = acsUiTemplate;
    }
}
