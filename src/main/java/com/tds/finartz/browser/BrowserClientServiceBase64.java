package com.tds.finartz.browser;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

interface BrowserClientServiceBase64 {
    @POST
    Call<String> callACS(@Header("Content-Type") String contentType, @Url String methodUrl, @Body String encodedCReq);
}
