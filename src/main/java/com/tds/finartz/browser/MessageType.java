package com.tds.finartz.browser;


/**
 * MessageTYpe
 */
public enum MessageType {

    AUTHENTICATION_REQUEST("AReq"),
    AUTHENTICATION_RESPONSE("ARes"),
    CHALLENGE_REQUEST("CReq"),
    CHALLENGE_RESPONSE("CRes"),
    PREPARE_REQUEST("PReq"),
    PREPARE_RESPONSE("PRes"),
    RESULT_REQUEST("RReq"),
    RESULT_RESPONSE("RRes"),
    ERROR("Erro"); // Not a typo, fyi

    private final String type;

    /**
     * MessageType
     * @param type Type
     */
    MessageType(String type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return type;
    }

    public static MessageType get(String value){
        for(MessageType messageType: MessageType.values()){
            if (messageType.toString().equalsIgnoreCase(value)) return messageType;
        }
        throw new IllegalArgumentException("No message type of '"+value+"' found.");
    }
}
