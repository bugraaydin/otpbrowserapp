package com.tds.finartz.browser;

import java.util.HashMap;

public class MessageExtension{

    private String name;

    private String id;

    private String criticalityIndicator;

    private HashMap<String,String> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCriticalityIndicator() {
        return criticalityIndicator;
    }

    public void setCriticalityIndicator(String criticalityIndicator) {
        this.criticalityIndicator = criticalityIndicator;
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }
}

