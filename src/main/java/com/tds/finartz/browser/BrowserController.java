package com.tds.finartz.browser;

import okhttp3.OkHttpClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Controller
public class BrowserController {

    /**
     * {
     * "threeDSServerTransID":"8a880dc0-d2d2-4067-bcb1-b08d1690b26e",
     * "acsTransID":"d7c1ee99-9478-44a6-b1f2-391e29c6b340",
     * "messageType":"CReq",
     * "messageVersion":"2.1.0",
     * "sdkTransID":"b2385523-a66c-4907-ac3c-91848e8c0067",
     * "sdkCounterStoA":"001",
     * "deviceChannel":"02"
     * }
     */
    @RequestMapping("/phoneNumber")
    public String index(Model model) throws IOException {


        CReq cReq = new CReq();

        cReq.setThreeDSServerTransID("8a880dc0-d2d2-4067-bcb1-b08d1690b26e");
        cReq.setAcsTransID("d7c1ee99-9478-44a6-b1f2-391e29c6b340");
        cReq.setMessageType("CReq");
        cReq.setMessageVersion("2.1.0");
        cReq.setSdkTransID("b2385523-a66c-4907-ac3c-91848e8c0067");
        cReq.setSdkCounterStoA("001");
        cReq.setDeviceChannel("02");

        /*
        BrowserClientService browserClientService = getClient("http://localhost:8081/").create(BrowserClientService.class);
        Call<String> call = browserClientService.callACS("./",cReq);
        */


        BrowserClientServiceBase64 browserClientServiceBase64 = getClient("http://localhost:8081/").create(BrowserClientServiceBase64.class);

        String cReqJsonString = cReq.toString();

        String encodedCReq = Base64.getUrlEncoder().encodeToString(cReqJsonString.getBytes());

        Call<String> call = browserClientServiceBase64.callACS("text/plain","./",encodedCReq.toString());

        String result = call.execute().body();

        model.addAttribute("otpHtml",result);

        return "index";
    }


    @RequestMapping("/")
    public String phoneNumber(Model model) throws IOException {

        model.addAttribute("otpHtml","<form class=\"otpFrame\" action=\"http://localhost:8081/\" method=\"post\">\n" +
                "    <div id=\"section1\" class=\"section1\">\n" +
                "        <div id=\"section1left\" class=\"section1left\">\n" +
                "            <img src=\"https://www.avanskredi.com/files/uploads/news/default/yapi-kredi-uyardi-di-cf2121c97512b286b711.png\" class=\"bankImg\">\n" +
                "        </div>\n" +
                "        <div id=\"section1right\" class=\"section1right\">\n" +
                "            <img src=\"https://vignette.wikia.nocookie.net/logopedia/images/f/f4/NoVictor_%28Visa%29.png/revision/latest?cb=20120825092100\" class=\"cardNetworkImg\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div id=\"section2\" class=\"section2\">\n" +
                "        Sistemde kayıtlı 055X XXX XX XX cep telefonunuza kod gönderilecektir. Onaylıyor musunuz ?\n" +
                "    </div>\n" +
                "    <div class=\"section3\">\n" +
                "    </div>\n" +
                "    <div id=\"section4\" class=\"section4\">\n" +
                "        <button type=\"submit\" class=\"confirmButton commonButtonStyle\" name=\"creq\" value=\"eyJ0aHJlZURTU2VydmVyVHJhbnNJRCI6ICI4YTg4MGRjMC1kMmQyLTQwNjctYmNiMS1iMDhkMTY5MGIyNmUiLCJhY3NUcmFuc0lEIjogImQ3YzFlZTk5LTk0NzgtNDRhNi1iMWYyLTM5MWUyOWM2YjM0MCIsIm1lc3NhZ2VUeXBlIjogIkNSZXEiLCJtZXNzYWdlVmVyc2lvbiI6ICIyLjEuMCIsInNka1RyYW5zSUQiOiAiYjIzODU1MjMtYTY2Yy00OTA3LWFjM2MtOTE4NDhlOGMwMDY3Iiwic2RrQ291bnRlclN0b0EiOiAiMDAxIiwiZGV2aWNlQ2hhbm5lbCI6ICIwMiJ9\"> Gönder </button>\n" +
                "    </div>\n" +
                "</form> <style>\n" +
                "\n" +
                "    .section1{\n" +
                "        height:20%;\n" +
                "    }\n" +
                "    .section1left{\n" +
                "        height:100%;\n" +
                "        width: 50%;\n" +
                "        float:left;\n" +
                "    }\n" +
                "    .section1right{\n" +
                "        height:100%;\n" +
                "        width: 49%;\n" +
                "        float:left;\n" +
                "    }\n" +
                "    .section2{\n" +
                "        height:20%;\n" +
                "        font-size: 13px;\n" +
                "        padding: 5px;\n" +
                "    }\n" +
                "    .section3{\n" +
                "        height:20%;\n" +
                "    }\n" +
                "    .section4{\n" +
                "        height:25%;\n" +
                "    }\n" +
                "    .section5{\n" +
                "        height:15%;\n" +
                "    }\n" +
                "    .resendButton{\n" +
                "        background-color: #bbbbbb;\n" +
                "    }\n" +
                "    .confirmButton{\n" +
                "        background-color: #c1e4ae;\n" +
                "    }\n" +
                "    .commonButtonStyle{\n" +
                "        width: 50%;\n" +
                "        height: 25px;\n" +
                "        margin:10px;\n" +
                "    }\n" +
                "    .otpFrame{\n" +
                "        width: 270px;\n" +
                "        height: 380px;\n" +
                "        background-color: #f7f7f7;\n" +
                "        text-align: center;\n" +
                "        padding: 10px;\n" +
                "        border: black;\n" +
                "        border: 2px solid black;\n" +
                "    }\n" +
                "    .otpInput{\n" +
                "        width:70%;\n" +
                "        height: 30px;\n" +
                "        margin-top:20px;\n" +
                "        text-align: center;\n" +
                "    }\n" +
                "    .bankImg{\n" +
                "        height:50%;\n" +
                "        width: 40%;\n" +
                "        margin-top: 20px;\n" +
                "    }\n" +
                "    .cardNetworkImg{\n" +
                "        height:50%;\n" +
                "        width: 40%;\n" +
                "        margin-top: 20px;\n" +
                "    }\n" +
                "    .section5left{\n" +
                "        margin-top: 20px;\n" +
                "        float:left;\n" +
                "    }\n" +
                "    .section5Right{\n" +
                "        margin-top: 20px;\n" +
                "    }\n" +
                "    .section3Up{\n" +
                "\n" +
                "    }\n" +
                "    .section3Down{\n" +
                "        float: right;\n" +
                "        width: 61%;\n" +
                "        padding: 6px;\n" +
                "        font-size: 13px;\n" +
                "    }\n" +
                "</style>");

        return "phoneNumber";
    }



    public static Retrofit getClient(String baseUrl){
        Retrofit retrofit;
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit;
    }
}
